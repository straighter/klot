"use strict";

import klot from "../src";

describe("initialization", () => {
  test("should throw an error when initialized empty", () =>
    expect(() => (klot as any)()).toThrow());

  test("should throw an error when initialized without a locale map (undefined)", () =>
    expect(() => (klot as any)("en")).toThrow());

  test("should throw an error when initialized without a locale map (not an object)", () =>
    expect(() => (klot as any)("en", 0)).toThrow());

  test("should return a function when 'invalid' inputs are used", () =>
    expect(() => (klot as any)("", {})).toBeInstanceOf(Function));

  test("should return a function when 'valid' inputs are used", () =>
    expect(() => klot("en", { en: {} })).toBeInstanceOf(Function));
});

describe("translation", () => {
  const faultyTranslate = (klot as any)("en", {
    nl: {
      "Hello world": "Hallo, mooie wereld"
    }
  });

  const translate = klot("en", {
    en: {
      "Hello world": "Hello, beautiful world",
      Multiple: { one: "two", two: "three" }
    }
  });

  test("should return an empty string if no input is given", () => {
    expect((translate as any)()).toEqual("");
  });

  test("should return an empty string if the requested locale isn't available", () => {
    expect(faultyTranslate("Hello world")).toEqual("");
  });

  test("should return the given input if the translation is not available", () => {
    expect((translate as any)("Hello")).toEqual("Hello");
  });

  test("should return the actual translation if the translation is available", () => {
    expect(translate("Hello world")).toEqual("Hello, beautiful world");
  });

  test("should return a function to transform the input if the translation is an object", () => {
    expect(translate("Multiple")).toBeInstanceOf(Function);
  });

  test("should return the transformed translation when a correct transform function is used", () => {
    expect(translate("Multiple")(data => data.one)).toEqual("two");
  });
});

describe("interpolation", () => {
  const translate = klot(
    "en",
    {
      en: {
        "T-shirt: {color}": "T-shirt: {color}",
        Strawberries: {
          none: "No strawberries",
          singular: "One strawberry",
          plural: "{amount} strawberries"
        }
      }
    },
    {
      values: { amount: 4, color: "red" }
    }
  );

  test("should globally interpolate a not found translation", () => {
    expect(translate("T-shirt: {color}")).toEqual("T-shirt: red");
  });

  test("should locally interpolate a not found translation", () => {
    expect(
      translate("T-shirt: {color}", { values: { color: "blue" } })
    ).toEqual("T-shirt: blue");
  });

  test("should globally interpolate the translation", () => {
    expect(translate("T-shirt: {color}")).toEqual("T-shirt: red");
  });

  test("should locally interpolate the translation", () => {
    expect(
      translate("T-shirt: {color}", { values: { color: "blue" } })
    ).toEqual("T-shirt: blue");
  });

  test("should interpolate in a transformation function", () => {
    expect(
      translate("Strawberries", { values: { amount: 0 } })((input, options) =>
        options.values.amount > 1 ? input.plural : input.none
      )
    ).toEqual("No strawberries");
  });

  test("should globally interpolate a transformed translation", () => {
    expect(
      translate("Strawberries")((input, options) =>
        options.values.amount > 1 ? input.plural : input.none
      )
    ).toEqual("4 strawberries");
  });
});
