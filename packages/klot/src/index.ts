"use strict";

export type KlotOptions = {
  interpolationRegex?: RegExp;
  values?: { [key: string]: string | number };
};

const DEFAULT_KLOT_OPTIONS = {
  interpolationRegex: /{([\w]+)}/g,
  values: {}
};

export default function klotFactory<
  Language extends keyof LocaleMap,
  LocaleMap extends {
    [key: string]: { [key: string]: string | { [key: string]: string } };
  },
  GlobalKlotOptions extends Partial<KlotOptions> = Partial<KlotOptions>
>(language: Language, locales: LocaleMap, globalOptions?: GlobalKlotOptions) {
  if (typeof language !== "string") {
    throw new Error("Language should be a string.");
  }

  if (typeof locales !== "object") {
    throw new Error("Locales should be of object");
  }

  const factoryOptions = {
    ...DEFAULT_KLOT_OPTIONS,
    ...globalOptions
  };

  const locale = locales[language];

  return function translate<
    Input extends keyof LocaleMap[Language] = string,
    LocalKlotOptions extends Partial<KlotOptions> = KlotOptions
  >(
    input: Input & string,
    localOptions?: LocalKlotOptions
  ): LocaleMap[Language][Input] extends string
    ? LocaleMap[Language][Input]
    : LocaleMap[Language][Input] extends { [key: string]: string }
    ? (
        fn: (
          transformableInput: LocaleMap[Language][Input],
          klotOptions: GlobalKlotOptions & LocalKlotOptions
        ) => LocaleMap[Language][Input][keyof LocaleMap[Language][Input]]
      ) => LocaleMap[Language][Input][keyof LocaleMap[Language][Input]]
    : Input extends string
    ? Input
    : "" {
    const options = {
      ...factoryOptions,
      ...localOptions
    };

    if (input && locale) {
      const translation = locale[input];

      if (typeof translation === "string") {
        return interpolate(translation, options) as any;
      }

      if (typeof translation === "object") {
        return function transform(
          fn: (
            transformableInput: LocaleMap[Language][Input],
            klotOptions: KlotOptions
          ) => LocaleMap[Language][Input][keyof LocaleMap[Language][Input]] &
            string
        ) {
          return interpolate(fn(translation, options), options);
        } as any;
      }

      return interpolate(input, options) as any;
    }

    return "" as any;
  };
}

function interpolate<
  T extends string = string,
  O extends Required<KlotOptions> = Required<KlotOptions>
>(text: T, options: O) {
  return text.replace(
    options.interpolationRegex,
    (_, valueName) => `${options.values[valueName] || ""}`
  );
}
